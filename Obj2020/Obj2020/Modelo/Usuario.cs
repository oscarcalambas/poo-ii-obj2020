﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Obj2020

{
   public class Usuario
    {
        public String Nombre { get; set; }
        public String Apellido { get; set; }
        public String User_name { get; set; }
        public String Correo { get; set; }
        public String Edad { get; set; }
    }
}
