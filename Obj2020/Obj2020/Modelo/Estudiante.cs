﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Obj2020
{
    public class Estudiante 
    {
        public String Nombre { get; set; }
        public String Apellido { get; set; }
        public String Materia { get; set; }
        public String Docente { get; set; }
        public String Nombre_Completo { get { return Nombre + " " + Apellido; } }
      
       
        public double Nota1 { get; set; }
        public double Nota2 { get; set; }
        public double Nota3 { get; set; }
        public double Nota { get { return ((Nota1 * 0.3) + (Nota2 * 0.3) + (Nota3 * 0.4)); } }
    }
}