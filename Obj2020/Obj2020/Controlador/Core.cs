﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Obj2020
{
    public static class Core
    {

        //SECIÓN COLORES
        public static Color Navegacion { get; } = Color.FromHex("#DFDFDF");
        public static Color Botones { get; } = Color.FromHex("#291E1E");
        public static Color BotonesFondo { get; } = Color.FromHex("#786C6C");
        public static Color Textos { get; } = Color.FromHex("#000000");
        public static Color Titulos { get; } = Color.FromHex("#000000");
        public static Color Principal { get; } = Color.FromHex("#808080");
        public static Color Textos_Especiales { get; } = Color.FromHex("#000000");

     

        //SECCIÓN IMAGENES/ICONOS

        public static ImageSource icono_salir { get; } = ImageSource.FromFile("icono_salir.png");

        public static ImageSource icono_atras { get; } = ImageSource.FromFile("icono_atras.png");

        //Imagen de icono UTAP
        public static ImageSource icono_utap { get; } = ImageSource.FromFile("icono_utap.png");
    }

}