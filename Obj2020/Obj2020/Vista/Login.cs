﻿
//using Obj2020.Vista;
using Obj2020;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;

namespace Obj2020
{
    public class Login : ContentPage
    {
        RelativeLayout vista_principalLogin;
        Image icono_utap;
        Label Bienvenida;
        Entry Documento, codigo_estudiante, contraseña;
        Button Botonacceder;
        Button Recuperar_contraseña;
        bool Respuesta;
        public Login()
        {
            CrearVistas();
            AgregarVistas();
            AgregarEventos();
        }
        void CrearVistas()
        {
            vista_principalLogin = new RelativeLayout();
            //INSTANCIAMOS LA IMAGEN
            //ELIMINAR BARRA SUPERIOR
            NavigationPage.SetHasNavigationBar(this, false);
            Documento = new Entry
            {
                Text = "",
                Placeholder = "          No  Documento          ",
                FontSize = 21,
                TextColor = Core.Textos,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            codigo_estudiante = new Entry
            {
                Text = "",
                Placeholder = "        Codigo Estudiante        ",
                FontSize = 21,
                TextColor = Core.Textos,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            contraseña = new Entry
            {
                Text = "",
                Placeholder = "               Contraseña                ",
                FontSize = 21,
                TextColor = Core.Textos,
                HorizontalTextAlignment = TextAlignment.Center,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                IsPassword = true
            };

            Botonacceder = new Button
            {
                BackgroundColor = Core.Botones,
                Text = "Accede a la APP ",
                CornerRadius = 1,
                TextColor = Color.White
            };

            icono_utap = new Image
            {
                Source = Core.icono_utap
            };

            Bienvenida = new Label
            {
                Text = "Bienvenido",
                FontSize = 30,
                TextColor = Core.Textos,
                HorizontalTextAlignment = TextAlignment.Center
            };

            Recuperar_contraseña = new Button
            {
                BackgroundColor = Core.Navegacion,
                Text = "¿ Olvidaste Contraseña ? ",
                CornerRadius = 1,
                TextColor = Color.Blue
            };
            //COLOR DE FONDO 
            BackgroundColor = Core.Navegacion;
        }
        void AgregarVistas()
        {
            vista_principalLogin.Children.Add(Bienvenida,
            Constraint.RelativeToParent((p) => { return 0; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return p.Height * 0.190; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width; })); //W: ANCHO DEL CONTROL

            vista_principalLogin.Children.Add(Documento,
            Constraint.RelativeToParent((p) => { return 0; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return p.Height * 0.500; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width; }), //W: ANCHO DEL CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.060; })); //H: ALTO DEL CONTROL

            vista_principalLogin.Children.Add(codigo_estudiante,
            Constraint.RelativeToParent((p) => { return 0; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return p.Height * 0.550; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width ; }), //W: ANCHO DEL CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.060; })); //H: ALTO DEL CONTROL

            vista_principalLogin.Children.Add(contraseña,
            Constraint.RelativeToParent((p) => { return 0; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return p.Height * 0.6; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width ; }), //W: ANCHO DEL CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.060; })); //H: ALTO DEL CONTROL

            vista_principalLogin.Children.Add(Botonacceder,
            Constraint.RelativeToParent((p) => { return p.Width * 0.176; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return p.Height * 0.7; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width * 0.666; }), //W: ANCHO DEL CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.066; })); //H: ALTO DEL CONTROL

            vista_principalLogin.Children.Add(Recuperar_contraseña,
            Constraint.RelativeToParent((p) => { return 0; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return p.Height * 0.8; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width * 0.956; }), //W: ANCHO DEL CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.151; })); //H: ALTO DEL CONTROL

            vista_principalLogin.Children.Add(icono_utap,
            Constraint.RelativeToParent((p) => { return 0; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return 0; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width; }), //W: ANCHO DEL CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.151; })); //H: ALTO DEL CONTROL

            Content = vista_principalLogin;
        }

        void AgregarEventos()
        {
            Botonacceder.Clicked += Botonacceder_Clicked;
            Recuperar_contraseña.Clicked += Recuperar_contraseña_Clicked;
        }
        private async void Recuperar_contraseña_Clicked(object sender, EventArgs e)
        {
            //Respuesta = await DisplayAlert("Notificación", "¿Deseas recuperar tu contraseña?", "Aceptar","Cancelar");
            await Navigation.PushAsync(new RecuperarContraseña());       
        }
        private async void Botonacceder_Clicked(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(Documento.Text))
            {
                await this.DisplayAlert("Notificacion", "No se ha ingresado el Documento", "Aceptar");
            }
            else
            {
                if (Documento.Text.Length < 8)
                {
                    await this.DisplayAlert("Notificacion", "El documento debe tener minimo 8 digitos", "Aceptar");
                }
                else
                {
                    if(String.IsNullOrWhiteSpace(codigo_estudiante.Text))
                    {
                        await this.DisplayAlert("Notificacion", "Ingrese el codigo de estudiante", "Aceptar");
                    }
                    else
                    { 
                        if(!codigo_estudiante.Text.ToCharArray().All(char.IsDigit))
                        {
                            await this.DisplayAlert("Notificacion", "El código estudiante debe ser numerico", "Aceptar");
                        }
                        else
                        {
                            if (String.IsNullOrWhiteSpace(contraseña.Text))
                            {
                                await this.DisplayAlert("Notificación", "Contraseña no debe estar vacia", "Aceptar");
                            }
                            else
                            {
                                await Navigation.PushAsync(new MasterPage());
                                Navigation.RemovePage(this);
                            }
                        }
                    }
                }
            }
        }
    }
}











