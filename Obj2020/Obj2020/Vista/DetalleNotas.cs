﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xamarin.Forms;

namespace Obj2020
{
   public class DetalleNotas : ContentPage     
    {
        Estudiante Global;
        RelativeLayout VistaSecundaria;
        BoxView barra_navegacion;
        Image icono_atras;
        TapGestureRecognizer tap_gesto_atras;
        Label Detalle_notas,Corte1, Corte2, Corte3, Resultado_final, NotaFinal;
        Button BotonNota1, BotonNota2, BotonNota3;
        public DetalleNotas(Estudiante NotaFinal)
        {
            Global = NotaFinal;
            NavigationPage.SetHasNavigationBar(this, false);
            CrearVistas();
            AgregarVistas();
            AgregarEventos();
        }
        void CrearVistas()
        {
            VistaSecundaria = new RelativeLayout();

            barra_navegacion = new BoxView
            {
                BackgroundColor = Core.Navegacion

            };

            icono_atras = new Image
            {
                Source = Core.icono_atras,
            };

            tap_gesto_atras = new TapGestureRecognizer();

            icono_atras = new Image
            {
                Source = Core.icono_atras

            };

            Detalle_notas = new Label
            {
                Text = "Detalle Notas",
                FontSize = 21,
                TextColor = Core.Textos,
                HorizontalTextAlignment = TextAlignment.Center
            };

            BotonNota1 = new Button
            {
                Text = Global.Nota1.ToString(),
                BackgroundColor = Core.Botones,
                FontSize = 35,
                CornerRadius = 20,
                TextColor = Color.White
            };

            BotonNota2 = new Button
            {
                Text = Global.Nota2.ToString(),
                BackgroundColor = Core.Botones,
                FontSize = 35,
                CornerRadius = 20,
                TextColor = Color.White
            };

            BotonNota3 = new Button
            {
                Text = Global.Nota3.ToString(),
                BackgroundColor = Core.Botones,
                FontSize = 35,
                CornerRadius = 20,
                TextColor = Color.White
            };

            NotaFinal = new Label
            {
                Text = Global.Nota.ToString(),
                FontSize = 35,
                TextColor = Core.Textos,
                HorizontalTextAlignment = TextAlignment.Center
            };

            Corte1 = new Label
            {
                Text = "Primer Corte",
                FontSize = 15,
                TextColor = Core.Textos,
                HorizontalTextAlignment = TextAlignment.Center

            };

            Corte2 = new Label
            {
                Text = "Segundo Corte",
                FontSize = 15,
                TextColor = Core.Textos,
                HorizontalTextAlignment = TextAlignment.Center,

            };

            Corte3 = new Label
            {
                Text = "Tercer Corte",
                FontSize = 15,
                TextColor = Core.Textos,
                HorizontalTextAlignment = TextAlignment.Center,
            };

            Resultado_final = new Label
            {
                Text = "Nota Final",
                TextColor = Core.Textos,
                FontSize = 20,
                HorizontalTextAlignment = TextAlignment.Center,
            };
            //Damos color al fondo 
            BackgroundColor = Core.Navegacion;
        }
        void AgregarVistas()
        {
            VistaSecundaria.Children.Add(barra_navegacion,
            Constraint.RelativeToParent((p) => { return 0; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return 0; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width; }), //W: ANCHO DEL CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.083; })); //H: ALTO DEL CONTROL

            VistaSecundaria.Children.Add(Detalle_notas,
            Constraint.RelativeToParent((p) => { return 0; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return p.Height * 0.022; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width; })); //W: ANCHO DEL CONTROL

            VistaSecundaria.Children.Add(icono_atras,
            Constraint.RelativeToParent((p) => { return 0; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return 0; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width * 0.149; }), //W: ANCHO DEL CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.083; })); //H: ALTO DEL CONTROL

            VistaSecundaria.Children.Add(BotonNota1,
            Constraint.RelativeToParent((p) => { return p.Width * 0.106; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return p.Height * 0.191; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width * 0.309; }), //W: ANCHO DEL CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.173; })); //H: ALTO DEL CONTROL

            VistaSecundaria.Children.Add(BotonNota2,
            Constraint.RelativeToParent((p) => { return p.Width * 0.597; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return p.Height * 0.191; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width * 0.309; }), //W: ANCHO DEL CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.173; })); //H: ALTO DEL CONTROL

            VistaSecundaria.Children.Add(BotonNota3,
            Constraint.RelativeToParent((p) => { return p.Width * 0.349; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return p.Height * 0.428; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width * 0.309; }), //W: ANCHO DEL CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.173; })); //H: ALTO DEL CONTROL

            VistaSecundaria.Children.Add(NotaFinal,
            Constraint.RelativeToParent((p) => { return p.Width * 0.352; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return p.Height * 0.820; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width * 0.309; }), //W: ANCHO DEL CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.061; })); //H: ALTO DEL CONTROL

            VistaSecundaria.Children.Add(Corte1,
            Constraint.RelativeToParent((p) => { return p.Width * 0.122; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return p.Height * 0.1619; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width * 0.28; }), //W: ANCHO DEL CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.029; })); //H: ALTO DEL CONTROL

            VistaSecundaria.Children.Add(Corte2,
            Constraint.RelativeToParent((p) => { return p.Width * 0.613; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return p.Height * 0.1619; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width * 0.28; }), //W: ANCHO DEL CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.029; })); //H: ALTO DEL CONTROL

            VistaSecundaria.Children.Add(Corte3,
            Constraint.RelativeToParent((p) => { return p.Width * 0.365; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return p.Height * 0.398; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width * 0.28; }), //W: ANCHO DEL CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.029; })); //H: ALTO DEL CONTROL

            VistaSecundaria.Children.Add(Resultado_final,
            Constraint.RelativeToParent((p) => { return p.Width * 0.277; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return p.Height * 0.790; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width * 0.458; }), //W: ANCHO DEL CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.0614; })); //H: ALTO DEL CONTROL

            Content = VistaSecundaria;
        }
        void AgregarEventos()
        {
            icono_atras.GestureRecognizers.Add(tap_gesto_atras);
            tap_gesto_atras.Tapped += Tap_gesto_atras_Tapped;
        }
        private async void Tap_gesto_atras_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MasterPage());
            Navigation.RemovePage(this);
        }
    }
}



    

