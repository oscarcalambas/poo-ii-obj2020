﻿using Obj2020;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Xamarin.Forms;

namespace Obj2020
{
    public class PaginaPrincipal : ContentPage
    {
        //CREAMOS NUESTRA VISTA
        RelativeLayout vista_principal;
        //CREAMOS NUESTRA BARRA DE NAVEGACIÓN
        BoxView barra_navegacion;
        //INVOCO LA IMAGEN
        Image icono_salir;
        Image icono_atras;
        Label titulo_pagina;
        TapGestureRecognizer tap_gesto_atras;
        List<Estudiante> lista_estudiantes;
        ListView ListViewEstudiantes;
        public PaginaPrincipal()
        {
            // NavigationPage.SetHasNavigationBar(this, false);
            CrearVistas();
            AgregarVistas();
            AgregarEventos();
        }
        void CrearVistas()
        {
            Title = "Pagina Principal";
            tap_gesto_atras = new TapGestureRecognizer();
            
            vista_principal = new RelativeLayout();

            lista_estudiantes = new List<Estudiante>();
            LlenadoDeEstudiantesQuemados();
            //INSTANCIAMOS NUESTRA BARRA DE NAVEGACIÓN
            barra_navegacion = new BoxView
            //INSTANCIAMOS LA IMAGEN
            {
                BackgroundColor = Core.Navegacion
            };
            //  icono_atras = new Image
            //    {

            //      Source = Core.icono_atras,
            //      IsVisible = false

            //};
            tap_gesto_atras = new TapGestureRecognizer();

            titulo_pagina = new Label
            {

                Text = "pagina Principal",
                FontSize = 21,
                TextColor = Core.Textos,
                HorizontalTextAlignment = TextAlignment.Center
                //};

                //icono_salir = new Image
                //{

                //Source = Core.icono_salir,
                //IsVisible = true
            };

            //icono_atras = new Image
            //{

            //    Source = Core.icono_atras,
            //    //IsVisible = true
            //};


            ListViewEstudiantes = new ListView
            {
                ItemsSource = lista_estudiantes,
                ItemTemplate = new DataTemplate(typeof(CeldaEstudiantes)),
                RowHeight = 80
            };
            //Damos color al fondo 
            BackgroundColor = Core.Navegacion;
        }
        void AgregarVistas()
        {
            //CODIGO PAR DAR PARAMETROS Y MEDIDAS AL LO QUE VA DENTRO DE LA VISTA
            // icono_atras.GestureRecognizers.Add(tap_gesto_atras);

            //vista_principal.Children.Add(barra_navegacion,
            // Constraint.RelativeToParent((p) => { return 0; }), //x:POSICIÓN  
            //Constraint.RelativeToParent((p) => { return 0; }),  //y:POSICIÓN
            // Constraint.RelativeToParent((p) => { return p.Width; }), //W: ANCHO DEL CONTROL
            // Constraint.RelativeToParent((p) => { return p.Height * 0.083; })); //H: ALTO DEL CONTROL

            //vista_principal.Children.Add(titulo_pagina,
            //Constraint.RelativeToParent((p) => { return 0; }), //x:POSICIÓN  
            //Constraint.RelativeToParent((p) => { return p.Height * 0.022; }),  //y:POSICIÓN
            //Constraint.RelativeToParent((p) => { return p.Width; })); //W: ANCHO DEL CONTROL



            //vista_principal.Children.Add(icono_salir,
            //Constraint.RelativeToParent((p) => { return p.Width * 0.850; }), //x:POSICIÓN  
            //Constraint.RelativeToParent((p) => { return 0; }),  //y:POSICIÓN
            //Constraint.RelativeToParent((p) => { return p.Width * 0.149; }), //W: ANCHO DEL CONTROL
            // Constraint.RelativeToParent((p) => { return p.Height * 0.083; })); //H: ALTO DEL CONTROL

            // vista_principal.Children.Add(icono_atras,
            //Constraint.RelativeToParent((p) => { return 0; }), //x:POSICIÓN  
            //Constraint.RelativeToParent((p) => { return 0; }),  //y:POSICIÓN
            //Constraint.RelativeToParent((p) => { return p.Width * 0.149; }), //W: ANCHO DEL CONTROL
            //Constraint.RelativeToParent((p) => { return p.Height * 0.083; })); //H: ALTO DEL CONTROL

            vista_principal.Children.Add(ListViewEstudiantes,
                Constraint.RelativeToParent((p) => { return 0; }),                      // X= POSICION
                Constraint.RelativeToParent((p) => { return p.Height * 0; }),       // Y= POSICION
                Constraint.RelativeToParent((p) => { return p.Width; }),                // W= ANCHO DE CONTROL
                Constraint.RelativeToParent((p) => { return p.Height * 0.824; }));      // H= ALTO DE CONTROL

            //y:POSICIÓN
            //Constraint.RelativeToParent((p) => { return p.Width * 0.149; }), //W: ANCHO DEL CONTROL
            //  Constraint.RelativeToParent((p) => { return p.Height * 0.083; })); //H: ALTO DEL CONTROL

            Content = vista_principal;
        }
        void AgregarEventos()
        {
            //listViewEstudiantes.ItemSelected += ListViewEstudiantes_ItemSelected; 
            tap_gesto_atras.Tapped += Tap_gesto_atras_Tapped;

            ListViewEstudiantes.ItemSelected += ListViewEstudiantes_ItemSelected;
            ListViewEstudiantes.ItemTapped += ListViewEstudiantes_ItemTapped;
        }
        private async void ListViewEstudiantes_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            await Navigation.PushAsync(new DetalleNotas((Estudiante)e.Item));
            Navigation.RemovePage(this);
        }

        private async void Tap_gesto_atras_Tapped(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

        private void ListViewEstudiantes_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null;
        }

        void LlenadoDeEstudiantesQuemados()
        {
            lista_estudiantes.Add(new Estudiante
            {
                Nombre = "Oscar",
                Apellido = "Calambas",
                Docente = "Docente: Luis Fernando Zapata",
                Materia = "Electiva II",
                Nota1 = 5,
                Nota2 = 1,
                Nota3 = 4
            });

            lista_estudiantes.Add(new Estudiante
            {
                Nombre = "Manuel",
                Apellido = "Medrano",
                Docente = "Docente: Eliecer Mesias",
                Materia = "Base datos",
                Nota1 = 5,
                Nota2 = 5,
                Nota3 = 1
            });

            lista_estudiantes.Add(new Estudiante
            {
                Nombre = "Javier",
                Apellido = "Zapata",
                Docente = "Docente: David Lourido",
                Materia = "Web II",
                Nota1 = 1,
                Nota2 = 1,
                Nota3 = 5
            });
        }
    }
}