﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace Obj2020
{
    public class RecuperarContraseña : ContentPage
    {
        RelativeLayout Vista_Principal;
        Image icono_atras;
        TapGestureRecognizer tap_gesto_atras;
        Entry Correo;
        Button BotonRecuperar;
        Label EtiquetaRecuperarcontraseña;
        BoxView Barra_navegacion;
        public RecuperarContraseña()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            CrearVistas();
            AgregarVistas();
            AgregarEventos();
        }
        void CrearVistas()
        {
            Vista_Principal = new RelativeLayout();
      
            tap_gesto_atras = new TapGestureRecognizer();

            icono_atras = new Image
            {
                Source = Core.icono_atras
            };

            Barra_navegacion = new BoxView
            {
                BackgroundColor = Core.Navegacion
            };

            EtiquetaRecuperarcontraseña = new Label
            {
                Text = "Recuperar Contraseña",
                FontSize = 21,
                TextColor = Core.Textos,
                HorizontalTextAlignment = TextAlignment.Center,
            };

            Correo = new Entry
            {
                Text = "",
                Placeholder = " Correo Institucional",
                FontSize = 21,
                TextColor = Core.Textos,
                HorizontalTextAlignment = TextAlignment.Center,
                HorizontalOptions = LayoutOptions.Center
            };

            BotonRecuperar = new Button
            {
                BackgroundColor = Core.Botones,
                Text = " Recuperar ",
                CornerRadius = 1,
                TextColor = Color.White
            };

            //Damos color al fondo 
            BackgroundColor = Core.Navegacion;
        }
        void AgregarVistas()
        {
            //Agregamos gesto
            // icono_atras.GestureRecognizers.Add(tap_gesto_atras);
            Vista_Principal.Children.Add(Barra_navegacion,
            Constraint.RelativeToParent((p) => { return 0; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return 0; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width; }), //W: ANCHO DEL CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.083; })); //H: ALTO DEL CONTROL

            Vista_Principal.Children.Add(EtiquetaRecuperarcontraseña,
            Constraint.RelativeToParent((p) => { return 0; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return p.Height * 0.022; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width; })); //W: ANCHO DEL CONTROL

            Vista_Principal.Children.Add(BotonRecuperar,
            Constraint.RelativeToParent((p) => { return p.Width * 0.176; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return p.Height * 0.7; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width * 0.666; }), //W: ANCHO DEL CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.066; })); //H: ALTO DEL CONTROL

            Vista_Principal.Children.Add(Correo,
            Constraint.RelativeToParent((p) => { return p.Width * 0.173; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return p.Height * 0.4; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width * 0.666; }), //W: ANCHO DEL CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.1; })); //H: ALTO DEL CONTROL

            Vista_Principal.Children.Add(icono_atras,
            Constraint.RelativeToParent((p) => { return 0; }), //x:POSICIÓN  
            Constraint.RelativeToParent((p) => { return 0; }),  //y:POSICIÓN
            Constraint.RelativeToParent((p) => { return p.Width * 0.149; }), //W: ANCHO DEL CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.1; })); //H: ALTO DEL CONTROL

            Content = Vista_Principal;
        }
        void AgregarEventos()
        {
            icono_atras.GestureRecognizers.Add(tap_gesto_atras);
            tap_gesto_atras.Tapped += Tap_gesto_atras_Tapped;
            BotonRecuperar.Clicked += BotonRecuperar_Clicked;            
        }

        private async void Tap_gesto_atras_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Login());
            Navigation.RemovePage(this);
        }

        private async  void BotonRecuperar_Clicked(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(Correo.Text))
            {
                await this.DisplayAlert("Advertencia", "El campo del correo electronico es obligatorio.", "OK");
            }
            else
            {
                //Valida que el formato del correo sea valido
                bool isEmail = Regex.IsMatch(Correo.Text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                if (!isEmail)
                {
                    await this.DisplayAlert("Advertencia", "El formato del correo electrónico es incorrecto, revíselo e intente de nuevo.", "OK");
                }
                else
                {
                    await this.DisplayAlert("Notificación", "Se ha generado una nueva contraseña", "Aceptar");
                }
            }
            //if (String.IsNullOrWhiteSpace(Correo.Text))
            //{
            //    await this.DisplayAlert("Notificación", "No se ha ingresado el Correo", "Aceptar");
            //}
            //else
            //{
            //    //Valida que el formato del correo sea valido
            //    bool isEmail = Regex.IsMatch(Correo.Text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
            //    if (!isEmail)
            //    {
            //        await this.DisplayAlert("Notificación", "Se ha generado una nueva contraseña", "Aceptar");
            //    }
            //}
        }
    }
}








