﻿using System;
using Obj2020;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Obj2020
{
    public class CeldaEstudiantes : ViewCell
    {
        StackLayout vista_general_celda, vistaLabels;
        Label labelNombreEstudiante, labelMateria, labelDocente, labelNota;
        public CeldaEstudiantes()
        {
            vista_general_celda = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Margin = new Thickness(12, 0, 12, 0)
            };

            vistaLabels = new StackLayout
            {
                HorizontalOptions = LayoutOptions.StartAndExpand
            };
            labelNombreEstudiante = new Label{};
            labelMateria = new Label{};
            labelDocente = new Label{};
            labelNota = new Label
            {
                FontSize = 30,
                TextColor = Core.Textos_Especiales,
                VerticalOptions = LayoutOptions.Center
            };
            labelNombreEstudiante.SetBinding(Label.TextProperty, "Nombre_Completo");
            labelMateria.SetBinding(Label.TextProperty, "Materia");
            labelDocente.SetBinding(Label.TextProperty, "Docente");
            labelNota.SetBinding(Label.TextProperty, "Nota");

            vistaLabels.Children.Add(labelNombreEstudiante);
            vistaLabels.Children.Add(labelMateria);
            vistaLabels.Children.Add(labelDocente);

            vista_general_celda.Children.Add(vistaLabels);
            vista_general_celda.Children.Add(labelNota);

            View = vista_general_celda;
        }
    }
}





 