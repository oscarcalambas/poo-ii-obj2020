﻿using Obj2020;
using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Text;
using Xamarin.Forms;

namespace Obj2020
{
    public class VistaMenuMaster : ContentPage
    {
        BoxView CuadroMasterPage;
        RelativeLayout Vista;
        Label TituloCaja;

        public VistaMenuMaster()
        {

            Title = "Mi Vista";
            //StackLayout Vista = new StackLayout 
            RelativeLayout Vista = new RelativeLayout { BackgroundColor = Color.White };

            CuadroMasterPage = new BoxView
            {
                BackgroundColor = Core.Botones,
                CornerRadius = 10
            };

            TituloCaja = new Label
            {
                Text = "SOY UTAP",
                FontSize = 23,
                TextColor = Core.Principal,
                HorizontalTextAlignment = TextAlignment.Center
            };

            List<MasterMenu> Menu = new List<MasterMenu>
            {
                //new MasterMenu {Titulo="Inicio",Icono = "home.png", IconoVisible=true},
                new MasterMenu {Titulo="Mis Materias y Notas", Icono="anadir.png",IconoVisible=true},
                new MasterMenu {Titulo="Mi Documentacion",Icono="mi_app.png",IconoVisible=true},
                new MasterMenu {Titulo="Cerrar Sesion", Icono="cerrar.png",IconoVisible=true}
            };

            ListView listView = new ListView
            {
                ItemsSource = Menu,
                ItemTemplate = new DataTemplate(typeof(EstiloTemplate)),
                SeparatorVisibility = SeparatorVisibility.Default,
                RowHeight = 70
            };

            listView.ItemSelected += (sender, e) =>
            {
                ((ListView)sender).SelectedItem = null;
            };

            listView.ItemTapped += ListView_ItemTapped;

            //Vista.Children.Add(listView);

            Vista.Children.Add(listView,
            Constraint.RelativeToParent((p) => { return p.Width * 0.020; }),      // X= POSICION 
            Constraint.RelativeToParent((p) => { return p.Height * 0.280; }),    // Y= POSICION
            Constraint.RelativeToParent((p) => { return p.Width * 0.66; }),       // W= ANCHO DE CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.900; }));     // H= ALTO DE CONTROL

            Vista.Children.Add(CuadroMasterPage,
            Constraint.RelativeToParent((p) => { return p.Width * 0.17; }),      // X= POSICION 
            Constraint.RelativeToParent((p) => { return p.Height * 0.100; }),    // Y= POSICION
            Constraint.RelativeToParent((p) => { return p.Width * 0.66; }),       // W= ANCHO DE CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.099; }));     // H= ALTO DE CONTROL

            Vista.Children.Add(TituloCaja,
            Constraint.RelativeToParent((p) => { return p.Width * 0.17; }),      // X= POSICION 
            Constraint.RelativeToParent((p) => { return p.Height * 0.130; }),    // Y= POSICION
            Constraint.RelativeToParent((p) => { return p.Width * 0.66; }),       // W= ANCHO DE CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.099; }));     // H= ALTO DE CONTROL

            Content = Vista;
        }

        private async void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            switch (((MasterMenu)e.Item).Titulo)
            {
                //case "Mis Materias y Notas":
                //    break;

                case "Mis Materias y Notas":
                    break;

                case "Mi Documentacion":
                    break;

                case "Cerrar Sesion":
                    await app.Current.MainPage.Navigation.PushAsync(new Login());
                    Page pagina = app.Current.MainPage.Navigation.NavigationStack[0];
                    Navigation.RemovePage(pagina);
                    break;

                default:
                    break;
            }
        }
    }
}
